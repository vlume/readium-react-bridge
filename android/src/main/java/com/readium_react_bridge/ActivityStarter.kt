package com.readium_react_bridge

import android.content.Intent
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import nl.komponents.kovenant.android.startKovenant
import org.readium.r2.testapp.CatalogActivity
import javax.annotation.Nullable
import kotlin.coroutines.CoroutineContext

class ActivityStarter (reactContext: ReactApplicationContext): ReactContextBaseJavaModule(reactContext), CoroutineScope {
    val libraryIntent: Intent  = Intent(reactApplicationContext, CatalogActivity::class.java)

    companion object {
        lateinit var reactContext: ReactApplicationContext

        fun staticChange(index: Int, total: Int, url: String) {
            val params: WritableMap = Arguments.createMap();
            val splitedURL = url.split("/")
            params.putInt("index", index)
            params.putInt("total", total)
            params.putString("url", url)
            println("URLL " + url);

            sendEvent(reactContext,"pageChanged", params)
        }

        fun staticSaveProgression(progression: String) {
            val params: WritableMap = Arguments.createMap();
            params.putString("progression", progression)

            sendEvent(reactContext,"locationChanged", params)
        }

        fun staticSaveHighlight(params: WritableMap) {
            sendEvent(reactContext,"highlight", params)
        }

        private fun sendEvent(reactContext: ReactContext, eventName: String, @Nullable params: WritableMap) {
            reactContext
                    .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
                    .emit(eventName, params);
        }
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    override fun getName(): String {
        return "RNReadiumReactBridge"
    }

    @ReactMethod
    fun navigateToReadium(filePath: String, progression: String, note: String) {
        reactContext = reactApplicationContext
        startKovenant()
        println("PROGRESSION")
        println(progression)
        libraryIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        libraryIntent.putExtra("filePath", filePath)
        libraryIntent.putExtra("progression", progression)
        reactApplicationContext.startActivity(libraryIntent)
    }

}