
#import "RNReadiumReactBridge.h"
#import "RNReadiumReactBridge-Swift.h"

@implementation RNReadiumReactBridge

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE(RNReadiumReactBridge)

- (NSArray<NSString *> *)supportedEvents {
    return @[@"pageChanged", @"locationChanged", @"highlight"];
}

+ (void)staticChange {
    [instance1 sendEventWithName:@"pageChanged" body:@{@"index": @"0"}];
}

+ (void)staticProgressionChange {
    [instance1 sendEventWithName:@"locationChanged" body:@""];
}

+ (void)staticHighlight {
    [instance1 sendEventWithName:@"highlight" body:@""];
}

RCT_EXPORT_METHOD(navigateToReadium:(NSString*)filePath
                  progression:(NSString*)progression
                  highlights:(NSString*)highlights) {
    instance1 = self;
    printf("navigateToREADIUM \n");
    ActivityStarter *start = [[ActivityStarter alloc] init];
    [start navigateToReadium:filePath
                 progression:progression
                  highlights:highlights
                     handler:^(int index, int total, NSString *url) {
        [instance1 sendEventWithName:@"pageChanged"
                                body:@{@"index": [NSNumber numberWithInt:index],
                                       @"total": [NSNumber numberWithInt:total],
                                       @"url": url}];
    } handlerProgression:^(NSString *progress) {
        [instance1 sendEventWithName:@"locationChanged" body:progress];
    } handlerHighlight:^(NSString *highlight) {
        [instance1 sendEventWithName:@"highlight" body:highlight];
    }];
}

@end
