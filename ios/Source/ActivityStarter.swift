//
//  ActivityStarter.swift
//  r2-testapp-swift
//
//  Created by LeeJongMin on 2019/7/28.
//
//  Copyright 2019 European Digital Reading Lab. All rights reserved.
//  Licensed to the Readium Foundation under one or more contributor license agreements.
//  Use of this source code is governed by a BSD-style license which is detailed in the
//  LICENSE file present in the project repository where this source code is maintained.
//

import Foundation
import UIKit

@objc (ActivityStarter) class ActivityStarter: NSObject {
    
    static var app = try! AppModule()
    static var handler : ((Int, Int, String) -> Void)?
    static var handlerProgression : ((String) -> Void)?
    static var handlerHighlight : ((String) -> Void)?
    
    class func locationChanged(progression: String) {
        handlerProgression?(progression)
    }
    
    class func pageChanged(index: Int, total: Int, href: String?) {
        print(href ?? "")
        handler?(index, total, href ?? "")
    }
    
    class func add(highlight: String) {
        handlerHighlight?(highlight)
    }
    
    @objc func navigateToReadium(_ filePath: String,
                                 progression: String,
                                 highlights: String,
                                 handler: @escaping ((Int, Int, String) -> Void),
                                 handlerProgression: @escaping (String) -> Void,
                                 handlerHighlight: @escaping (String) -> Void) {
        ActivityStarter.handler = handler
        ActivityStarter.handlerProgression = handlerProgression
        ActivityStarter.handlerHighlight = handlerHighlight
    
        HighlightDataSource.data = parse(highlights)
        
        let filePath = URL(fileURLWithPath: filePath).absoluteString
        var books = try! BooksDatabase.shared.books.all()
        for book in books {
            if book.href == filePath {
                if let str = convertJson(string: progression) {
                    book.progression = str
                }
                open(book)
                return
            }
        }
        
        if ActivityStarter.app.library.library.addPublication(at: URL(string: filePath)!) {
            books = try! BooksDatabase.shared.books.all()
            for book in books {
                if book.href == filePath {
                    if let str = convertJson(string: progression) {
                        book.progression = str
                    }
                    open(book)
                    return
                }
            }
        }
    }
    
    func open(_ book: Book) {
        guard let (publication, container) = ActivityStarter.app.library.library.parsePublication(for: book) else {
            return
        }
        DispatchQueue.main.async {
            ActivityStarter.app.library.library.preparePresentation(of: publication, book: book, with: container)
            
            if let _ = UIApplication.topViewController() {
                ActivityStarter.app.reader.presentPublication(publication: publication, book: book, in: nil, completion: {})
            }
        }
    }
    
    private func convertJson(string: String) -> String? {
        let data = string.data(using: .utf8)
        do {
            if let str = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as? String {
                return str
            }
        }
        catch {
            return nil
        }
        return nil
    }
    
    private func parse(_ string: String) -> String {
        let data = string.data(using: .utf8)!
        do {
            if let json = try JSONSerialization.jsonObject(with: data,
                                                           options: .fragmentsAllowed) as? [String: Any] {
                return json["data"] as? String ?? ""
            } else {
                print("bad json")
                return ""
            }
        } catch let error {
            print(error.localizedDescription)
            return ""
        }
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        else if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
