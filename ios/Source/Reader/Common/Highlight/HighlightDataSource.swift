//
//HighlightDataSource.swift
//  r2-testapp-swift
//
//  Created by Senda Li on 2018/7/19.
//
//  Copyright 2018 European Digital Reading Lab. All rights reserved.
//  Licensed to the Readium Foundation under one or more contributor license agreements.
//  Use of this source code is governed by a BSD-style license which is detailed in the
//  LICENSE file present in the project repository where this source code is maintained.
//

import Foundation
import R2Shared
    
class HighlightDataSource: Loggable {
    static var data: String = ""
    
    let publicationID :String?
    private(set) var highlights = [HighlightData]()
    
    var removeHighlight: ((String) -> Void)?
    
    init() {
        self.publicationID = nil
        self.reloadHighlights()
        parse(HighlightDataSource.data)
    }
    
    init(publicationID: String) {
        self.publicationID = publicationID
        self.reloadHighlights()
        parse(HighlightDataSource.data)
    }
    
    func parse(_ string: String) {
        let data = string.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data,
                                                                options: .fragmentsAllowed) as? [[String: Any]] {
                for json in jsonArray {
                    if let highlight = HighlightData(json: json) {
                        highlights.append(highlight)
                    }
                }
            } else {
                print("bad json")
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func reloadHighlights() {
        highlights.sort { (b1, b2) -> Bool in
            let locations1 = b1.locator.locations
            let locations2 = b2.locator.locations
            if b1.resourceIndex == b2.resourceIndex {
                if let position1 = locations1.position, let position2 = locations2.position {
                    return position1 < position2
                } else if let progression1 = locations1.progression, let progression2 = locations2.progression {
                    return progression1 < progression2
                }
            }
            return b1.resourceIndex < b2.resourceIndex
        }
    }
    
    var count: Int {
        return highlights.count
    }
    
    func highlight(at index: Int) -> HighlightData? {
        guard highlights.indices.contains(index) else {
            return nil
        }
        return highlights[index]
    }
    
    func getHighlights(by href: String? = nil) -> [HighlightData]? {
        var newHighlights: [HighlightData] = []
        for highlight in highlights {
            if let href = href, highlight.locator.href == href {
                newHighlights.append(highlight)
            }
        }
        
        return highlights
    }
    
    func getHighlight(by id: String) -> HighlightData? {
        for highlight in highlights {
            if highlight.highlightFrameID == id {
                return highlight
            }
        }
        
        return nil
    }
    
    func update(highlight: HighlightData) -> Bool? {
        sendEvent()
        return true
    }
    
    func add(highlight: HighlightData) -> Bool {
        let h = highlight
        h.id = UUID().uuidString
        highlights.append(highlight)
        reloadHighlights()
        sendEvent()
        
        return true
    }
    
    func removeHighlight(at index: Int) -> Bool {
        if index < 0 || index >= highlights.count {
            return false
        }
        
        let highlight = highlights[index]
        removeHighlight?(highlight.highlightFrameID)
        highlights.remove(at: index)
        reloadHighlights()
        sendEvent()
        return true
    }
    
    @discardableResult
    func remove(highlight: HighlightData) -> Bool {
        highlights = highlights.filter { $0.id != highlight.id}
        reloadHighlights()
        sendEvent()
        return true
    }
    
    func convertJSONString(_ jsonArray: [[String: Any]]) -> String? {
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: jsonArray,
                                                            options: .fragmentsAllowed)
            if let str = String(data: jsonData, encoding: .utf8) {
                return str
            }
        } catch let error {
            print("Array convertIntoJSON - \(error.localizedDescription)")
        }
        return nil
    }
    
    func sendEvent() {
        var jsonArray: [[String: Any]] = []
        
        for highlight in highlights {
            let json = highlight.toJson()
            jsonArray.append(json)
        }
        
        if let str = convertJSONString(jsonArray) {
            ActivityStarter.add(highlight: str)
        }
    }
}
