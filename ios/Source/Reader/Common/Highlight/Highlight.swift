//
//  Highlight.swift
//  r2-testapp-swift
//
//  Created by Taehyun Kim on 06.06.19.
//
//  Copyright 2019 Readium Foundation. All rights reserved.
//  Use of this source code is governed by a BSD-style license which is detailed
//  in the LICENSE file present in the project repository where this source code is maintained.
//

import Foundation
import UIKit
import R2Shared


class HighlightData {
    
    var annotation: String
    var color: String
    var style: String
    var annotationMarkStyle: String
    var selectionInfo: String
    var highlightFrameID: String
    var annotationID: String
    var id: String?
    var publicationID: String
    var resourceIndex: Int
    var creationDate: Date
    var locator: Locator
    
    init(id: String? = nil, publicationID: String = "", resourceIndex: Int = 0, locator: Locator, creationDate: Date = Date(), annotation: String = "", color:String = "", style:String = "", annotationMarkStyle:String = "", selectionInfo:String = "", highlightFrameID:String = "", annotationID: String = "") {
        
        self.annotation = annotation
        self.color = color
        self.style = style
        self.annotationMarkStyle = annotationMarkStyle
        self.selectionInfo = selectionInfo
        self.highlightFrameID = highlightFrameID
        self.annotationID = annotationID
        self.id = id
        self.publicationID = publicationID
        self.resourceIndex = resourceIndex
        self.locator = locator
        self.creationDate = creationDate
    }
    
    init?(json: [String: Any]) {
        guard let highlightID = json["highlightID"] as? String,
            let locatorText = json["locatorText"] as? String,
            let color = json["color"] as? String,
            let annotation = json["annotation"] as? String,
            let annotationMarkStyle = json["annotationMarkStyle"] as? String,
            let resourceIndex = json["resourceIndex"] as? Int,
            let highlightFrameID = json["highlightFrameID"] as? String,
            let publicationID = json["publicationID"] as? String,
            let creationDate = json["creationDate"] as? String,
            let style = json["style"] as? String else { return nil }
            
        self.annotation = annotation
        self.color = color
        self.style = style
        self.annotationMarkStyle = annotationMarkStyle
        self.selectionInfo = ""
        self.highlightFrameID = highlightFrameID
        self.annotationID = ""
        self.id = highlightID
        self.publicationID = publicationID
        self.resourceIndex = resourceIndex
        self.locator = Locator(href: "", type: "")
        
        do {
            guard let locator = try Locator(jsonString: locatorText) else { return nil }
            self.locator = locator
        } catch let error {
            print(error.localizedDescription)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M/dd/yy HH:mm"
        let date = dateFormatter.date(from: creationDate)
        
        self.creationDate = date ?? Date()
    }
    
    func toJson() -> [String: Any] {
        var json: [String: Any] = [:]
        
        json["highlightID"] = id ?? ""
        json["publicationID"] = publicationID
        json["style"] = style
        json["color"] = color
        json["annotation"] = annotation
        json["annotationMarkStyle"] = annotationMarkStyle
        json["resourceIndex"] = resourceIndex
        json["resourceHref"] = locator.href
        json["resourceType"] = locator.type
        json["location"] = locator.locations.jsonString ?? ""
        json["locatorText"] = locator.jsonString ?? ""
        json["creationDate"] = convertDateToString(creationDate)
        json["highlightFrameID"] = highlightFrameID
        
        return json
    }
    
    func convertDateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "M/dd/yy HH:mm"
        return formatter.string(from: date)
    }
}
