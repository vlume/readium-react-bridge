//
//  annotate.swift
//  r2-testapp-swift
//
//  Created by Taehyun on 24/06/2019.
//
//  Copyright 2019 European Digital Reading Lab. All rights reserved.
//  Licensed to the Readium Foundation under one or more contributor license agreements.
//  Use of this source code is governed by a BSD-style license which is detailed in the
//  LICENSE file present in the project repository where this source code is maintained.
//

import Foundation
import UIKit

class AnnotationViewController: UIViewController {
    
    var selectionText = ""
    var existingText = ""
    var newAnnotation = ""
    var id = ""
    
    let annotation: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let selectText: UITextView = {
        let tv = UITextView()
        tv.isEditable = false
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    private let separatorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(red: 172 / 255, green: 216 / 255, blue: 255 / 255, alpha: 1)
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private let doneButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("Done", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        annotation.becomeFirstResponder()
    }
    
    private func setup() {
        annotation.text = existingText
        selectText.text = selectionText
        selectText.textContainer.maximumNumberOfLines = 3
        selectText.textContainer.lineBreakMode = .byTruncatingTail
        selectText.centerVertically()
    }
        
    private func setupViews() {
        self.view.backgroundColor = .white
        self.view.addSubview(doneButton)
        doneButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        doneButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        doneButton.topAnchor.constraint(equalTo: self.view.topAnchor,
                                        constant: self.view.safeAreaTopInset + 5).isActive = true
        doneButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -15).isActive = true
        
        self.view.addSubview(separatorView)
        separatorView.widthAnchor.constraint(equalToConstant: 5).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 65).isActive = true
        separatorView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        separatorView.topAnchor.constraint(equalTo: doneButton.bottomAnchor, constant: 30).isActive = true
        
        self.view.addSubview(selectText)
        selectText.heightAnchor.constraint(equalToConstant: 65).isActive = true
        selectText.leadingAnchor.constraint(equalTo: separatorView.trailingAnchor, constant: 20).isActive = true
        selectText.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        selectText.topAnchor.constraint(equalTo: separatorView.topAnchor, constant: 0).isActive = true
        
        self.view.addSubview(annotation)
        annotation.leadingAnchor.constraint(equalTo: selectText.leadingAnchor, constant: 0).isActive = true
        annotation.trailingAnchor.constraint(equalTo: selectText.trailingAnchor, constant: 0).isActive = true
        annotation.topAnchor.constraint(equalTo: selectText.bottomAnchor, constant: 22).isActive = true
        annotation.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        
        doneButton.addTarget(self, action: #selector(dismis), for: .touchUpInside)
    }
    
    @objc private func dismis() {
        newAnnotation = annotation.attributedText.string
        dismiss(animated: true, completion: nil)
        
        let notification = Notification.Name(rawValue: "annotated")
        let param: [String: String] = ["text": newAnnotation, "id": id]
        
        NotificationCenter.default.post(name: notification, object: self, userInfo: param)
    }
}

extension UITextView {
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }
}

extension UIView {
    var safeAreaTopInset: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0
        }
        
        return 0
    }
    
    var safeAreaBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.bottom ?? 0
        }
        
        return 0
    }
}
