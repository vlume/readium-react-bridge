
#if __has_include("RCTBridgeModule.h")
//#import "RCTBridgeModule.h"
#import "RCTEventEmitter.h"
#else
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#endif
@class ActivityStarter;

@interface ActivityStarter : NSObject
- (void)navigateToReadium:(NSString*)str
              progression:(NSString*)progression
              highlights:(NSString*)highlights
                  handler:(void(^)(int,int,NSString*))handler
       handlerProgression:(void(^)(NSString*))handlerProgression
         handlerHighlight:(void(^)(NSString*))handlerHighlight;
@end


@interface RNReadiumReactBridge : RCTEventEmitter <RCTBridgeModule>
+ (void)staticChange;
+ (void)staticProgressionChange;
+ (void)staticHighlight;
@end
  
static RNReadiumReactBridge* instance1;
